# import shlex
import logging

import os
import sys
from invoke import Collection, task

# mypy: allow-untyped-defs


@task()
def update(c):
    c.run(
        r"""
        poetry update
        """
    )


@task(name="update-latest")
def update_latest(c, accept=False):
    c.run(
        f"""
        dasel select -f pyproject.toml -m 'tool.poetry.dev-dependencies.-' \\
            | sed 's/.*/&@latest/g' \\
            | xargs -n1 {"-t" if accept else "echo"} poetry add --dev
        dasel select -f pyproject.toml -m 'tool.poetry.dependencies.-' \\
            | grep -v '^python' \\
            | sed 's/.*/&@latest/g' \\
            | xargs -n1 {"-t" if accept else "echo"} poetry add
        """
    )


@task
def validate(c):
    c.run(
        r"""
        black --check ./src ./tests tasks.py || exit 1
        flake8 ./src ./tests tasks.py || exit 1
        mypy --strict --show-error-codes --show-error-context \
            ./tests ./src tasks.py || exit 1
        # nosetests -c nose.cfg tests.test_namespace:Namespace.test_prov
        nosetests -c nose.cfg || exit 1
        """
    )


@task(name="validate-fix")
def validate_fix(c):
    c.run(
        r"""
        black ./src ./tests tasks.py || exit 1
        """
    )


@task(name="install-editable")
def install_editable(c, escaped=False):
    escape_venv = r"""
    ## escape venv
    IFS=':' read -r -a PATH_ARRAY <<< "$PATH"
    IFS= readarray -d '' NEW_PATH_ARRAY \
        < <(printf "%s\000" "${PATH_ARRAY[@]}" | sed -z '/.venv[/]bin/d')
    PATH=$(IFS=:;echo "${NEW_PATH_ARRAY[*]}")
    unset VIRTUAL_ENV
    export PATH VIRTUAL_ENV
    """
    if not escaped:
        c.run(
            escape_venv
            + r"""
            ## uninstall global
            pip3 uninstall -y "$(poetry version | gawk '{ print $1 }')"
            """
        )
        c.run(
            r"""
            \rm -rv src/*.egg-info/
            ## uninstall in venv
            pip3 uninstall -y "$(poetry version | gawk '{ print $1 }')"
            \rm -rv dist/
            poetry build --format sdist \
                && tar --wildcards -xvf dist/*.tar.gz -O '*/setup.py' > setup.py \
                && pip3 install --prefix="${HOME}/.local/" --editable . || exit 1
            """
        )
    else:
        c.run(
            r"""
            rm -rv src/*.egg-info/
            ## uninstall in venv
            pip3 uninstall -y "$(poetry version | gawk '{ print $1 }')"

            """
            + escape_venv
            + r"""
            ## uninstall global
            pip3 uninstall -y "$(poetry version | gawk '{ print $1 }')"

            ## install
            \\rm -rv dist/
            poetry build --format sdist \
                && tar --wildcards -xvf dist/*.tar.gz -O '*/setup.py' > setup.py \
                && pip3 install --prefix="${HOME}/.local/" --editable . || exit 1
            """
        )


@task(pre=[validate])
def all(c):
    pass


@task
def clean(c):
    build_dir = c["build_dir"]
    os.path.exists(build_dir) and c.run(f"rm -rfv {build_dir}")


ns = Collection.from_module(sys.modules[__name__], name="")

ns.configure({"build_dir": "build"})
logging.debug("ns.task_names = %s", ns.task_names)
