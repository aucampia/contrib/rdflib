
.PHONY: validate
validate:
	poetry run black --check ./src ./tests
	poetry run flake8 ./src ./tests
	poetry run mypy --strict ./tests ./src
	poetry run pytest ./tests

.PHONY: validate-fix
validate-fix: validate
	poetry run black ./src ./tests
