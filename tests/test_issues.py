from SPARQLWrapper import SPARQLWrapper, RDFXML
from rdflib.term import URIRef, Literal
from rdflib.namespace import XSD
import rdflib.term
import decimal
import logging
import unittest
import sys
import os


class Issues(unittest.TestCase):
    def test_issue1314(self) -> None:

        endpoint_url = "https://query.wikidata.org/sparql"
        user_agent = "LINCS-https://lincsproject.ca//%s.%s" % (
            sys.version_info[0],
            sys.version_info[1],
        )
        sparql = SPARQLWrapper(endpoint_url, agent=user_agent)
        sparql.setReturnFormat(RDFXML)
        query = """CONSTRUCT { ?s ?p ?o } WHERE { VALUES ?s { wd:Q184832 } ?s ?p ?o }"""
        sparql.setQuery(query)
        results = sparql.query().convert()

        triples = set(
            results.triples(
                (None, URIRef("http://www.wikidata.org/prop/direct/P2201"), None)
            )
        )
        for triple in triples:
            (s, p, o) = triple
            logging.info("triple = %s", triple)
            logging.info("str(o) = %s", str(o))
            logging.info("o.value = %s/%s", type(o.value), o.value)
            logging.info("o.n3() = %s", o.n3())

        rdflib.term.bind(
            XSD.decimal,
            decimal.Decimal,
            constructor=decimal.Decimal,
            lexicalizer=lambda val: f"{val:f}",
            datatype_specific=True,
        )

        results = sparql.query().convert()
        triples = set(
            results.triples(
                (None, URIRef("http://www.wikidata.org/prop/direct/P2201"), None)
            )
        )
        for triple in triples:
            (s, p, o) = triple
            logging.info("triple = %s", triple)
            logging.info("str(o) = %s", str(o))
            logging.info("o.value = %s/%s", type(o.value), o.value)
            logging.info("o.n3() = %s", o.n3())


if __name__ == "__main__":
    logging.basicConfig(
        level=os.environ.get("PYTHON_LOGGING_LEVEL", logging.INFO),
        stream=sys.stderr,
        datefmt="%Y-%m-%dT%H:%M:%S",
        # format=(
        #     "%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
        #     "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"
        # ),
        format=(
            "%(message)s"
        ),
    )

    unittest.main()
