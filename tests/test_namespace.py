import unittest
import logging
import os
import sys
from pathlib import Path
from rdflib import Graph, URIRef
from rdflib.namespace import PROV
from nose.exc import SkipTest

SCRIPT_PATH = Path(__file__)


class Namespace(unittest.TestCase):
    def setUp(self) -> None:
        self.maxDiff = None

        return super().setUp()

    @unittest.expectedFailure
    def test_prov(self) -> None:
        graph = Graph()
        graph.parse(source=(SCRIPT_PATH.parent / "data" / "prov.ttl"), format="turtle")

        qres = graph.query(
            r"""
            PREFIX owl: <http://www.w3.org/2002/07/owl#>
            SELECT DISTINCT ?term ?ontology WHERE {
                ?term ?p [].
                # MINUS { ?term rdf:type owl:AnnotationProperty }
                FILTER isIRI(?term).
                FILTER strStarts(str(?term), "http://www.w3.org/ns/prov#").
                FILTER (str(?term) != "http://www.w3.org/ns/prov#")
                OPTIONAL { ?term rdfs:isDefinedBy ?ontology }
            }
            """
        )
        owl_terms = {}
        for row in qres:
            owl_terms[row["term"]] = row["ontology"]
        owl_term_set = set(owl_terms.items())

        provo_uri = URIRef("http://www.w3.org/ns/prov-o#")
        # provo_term_set is terms matching :
        #   [] rdfs:isDefinedBy  <http://www.w3.org/ns/prov-o#>
        provo_term_set = set([term for term in owl_term_set if term[1] == provo_uri])

        rdflib_term_set = set(
            [
                (PROV[termword], owl_terms.get(PROV[termword], None))
                for termword in dir(PROV)
            ]
        )
        self.assertLessEqual(provo_term_set, rdflib_term_set)
        self.assertGreaterEqual(owl_term_set, rdflib_term_set)


class Debug(unittest.TestCase):
    def setUp(self) -> None:
        self.maxDiff = None
        if not os.environ.get("DEBUGTESTS", ""):
            raise SkipTest("Skipping tests made for debugging")
        return super().setUp()

    def test_debug(self) -> None:
        graph = Graph()
        graph.parse(location=str(SCRIPT_PATH.parent / "data" / "prov.nt"), format="nt")
        qres = graph.query(
            r"""
            SELECT ?ontology (COUNT(?term) as ?termCount)  WHERE {
                ?term rdfs:isDefinedBy ?ontology
            }
            GROUP BY ?ontology
            """
        )
        for row in qres:
            logging.info("isDefinedBy = %s", row)

    def test_prov(self) -> None:
        graph = Graph()
        graph.parse(source=(SCRIPT_PATH.parent / "data" / "prov.ttl"), format="turtle")
        qres = graph.query(
            r"""
            PREFIX owl: <http://www.w3.org/2002/07/owl#>
            SELECT DISTINCT ?term ?ontology WHERE {
                { ?term ?p []. }
                UNION
                { [] ?term []. }
                UNION
                { [] ?p ?term. }
                # MINUS { ?term rdf:type owl:AnnotationProperty }
                FILTER isIRI(?term).
                FILTER strStarts(str(?term), "http://www.w3.org/ns/prov#").
                FILTER (str(?term) != "http://www.w3.org/ns/prov#")
                OPTIONAL { ?term rdfs:isDefinedBy ?ontology }
            }
            """
        )
        used_terms = {}
        for row in qres:
            used_terms[row["term"]] = row["ontology"]
        used_term_set = set(used_terms.items())
        qres = graph.query(
            r"""
            PREFIX owl: <http://www.w3.org/2002/07/owl#>
            SELECT DISTINCT ?term ?ontology WHERE {
                ?term ?p [].
                # MINUS { ?term rdf:type owl:AnnotationProperty }
                FILTER isIRI(?term).
                FILTER strStarts(str(?term), "http://www.w3.org/ns/prov#").
                FILTER (str(?term) != "http://www.w3.org/ns/prov#")
                OPTIONAL { ?term rdfs:isDefinedBy ?ontology }
            }
            """
        )
        owl_terms = {}
        for row in qres:
            owl_terms[row["term"]] = row["ontology"]
        owl_term_set = set(owl_terms.items())
        self.assertEqual(used_term_set, owl_term_set)


logging.basicConfig(
    level=os.environ.get("PYTHON_LOGGING_LEVEL", logging.INFO),
    stream=sys.stderr,
    datefmt="%Y-%m-%dT%H:%M:%S",
    format=(
        "%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
        "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"
    ),
)

if __name__ == "__main__":
    unittest.main()
